# FasTrak

## Usage

```bash
 python3 fastrak-nb.py -h                                      
Usage: fastrak-nb.py [OPTIONS]

  Routine to check the FasTrack I-15 NB lanes

Options:
  --phone-number TEXT  the text phone number
  --log-dir TEXT       the log output folder - default is the current folder
  -v, --verbose        verbose flag
  -h, --help           Show this message and exit.
```

## Text Service

```bash
curl "http://textbelt.com/text" -d number=xxxxxxxxxxx -d "message=this is where you put your message"
{"success":false,"error":"The hosted Textbelt endpoint now requires a 'key' param. Please go to https://textbelt.com to sign up for an API key or use the key \"textbelt\" for a free SMS per day. You may also choose to self-host and send unlimited SMS."}%
```
