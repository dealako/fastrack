"""
Routine to query FasTrack I-15 NB times.
"""

import os
import sys
from datetime import datetime
from logging import INFO, DEBUG, getLevelName

import click
import requests

import log


def send_text(num: str, msg: str):
    resp = requests.post('https://textbelt.com/text', {
        'phone': num,
        'message': msg,
        'key': 'textbelt-key',
    })
    print(resp.json())


@click.command(context_settings={'help_option_names': ['-h', '--help']})
@click.option('--phone-number', is_flag=False, type=click.STRING,
              help='the text phone number')
@click.option('--log-dir', is_flag=False, type=click.STRING,
              default='.',
              help='the log output folder - default is the current folder')
@click.option('-v', '--verbose', is_flag=True, help='verbose flag')
def main(phone_number, log_dir, verbose):
    """
    Routine to check the FasTrack I-15 NB lanes
    """
    if not phone_number:
        print('Missing --phone-number parameter')
        return

    if not os.path.isdir(log_dir):
        print(f'Log directory does not exist: \'{log_dir}\' '
              '- please create or adjust --log-dir parameter')
        return

    log_level = INFO
    if verbose:
        log_level = DEBUG

    start_time = datetime.now()
    logger = log.setup_custom_logger(
        'root',
        log_dir=log_dir,
        level=log_level,
        prefix=f'fastrak-nb-{os.environ.get("STAGE")}')

    logger.info(f'log-dir         : {log_dir}')
    logger.info(f'log level       : {getLevelName(log_level)}')

    try:
        r = requests.get('https://traffic.511sd.com/tgtolls/api/signs?_=1566977320790')
        r.raise_for_status()
    except requests.exceptions.HTTPError as err:
        msg = f'Could not get FastTrak info, error: {err}'
        logger.warning(msg)
        return None

    data = r.json()

    # "display": {
    #    "title": "SB - Entrance from SR 78",
    #    "tollSignType": "ADDCO_8",
    #    "displayData": {
    #       "dataFields": [...]
    #    "order": 15
    msg = ''
    for item in data:
        if item['id'].startswith('NB'):
            data_fields = item["display"]["displayData"]["dataFields"]
            if len(data_fields) == 2:
                msg += f'id: {item["id"]} - ${data_fields[0]} {data_fields[1]}min\n'
            if len(data_fields) == 4:
                msg += f'id: {item["id"]} - ${data_fields[0]} {data_fields[1]}min / ' \
                       f'${data_fields[2]} {data_fields[3]}min\n'

    send_text(phone_number, msg)

    logger.info('Finished - duration: {}'.format(datetime.now() - start_time))


if __name__ == "__main__":
    sys.exit(main())
